<?php

require_once 'Connect.php';

$table = new Connect();

if(isset($_POST['create']) && $_POST['create'] == 'table'){

    echo "<pre>";
    print_r($_POST);
    echo "</pre>";


    //$table = new Connect();

    $table->tableName = $_POST['tableName'];
    $table->createQuery();


    for($i = 1; isset($_POST['columnName'.$i]) && $i <= count($_POST);  $i++){
        $table->addColumn( $_POST['columnName'.$i], $_POST['columnType'.$i], $_POST['columnLength'.$i]);

    }

    for($i = 1; isset($_POST['columnName'.$i]) && $i <= count($_POST);  $i++){

        if(isset($_POST['applyCheck'.$i]) && $_POST['applyCheck'.$i] == 'on'){
            $table->addCheck($_POST['checkVal'.$i]);
        }
    }


    for($i = 1; isset($_POST['columnCONST'.$i]) && $i <= count($_POST);  $i++){
        if(isset($_POST['columnCONST'.$i]) && $_POST['columnCONST'.$i] == 'PK'){
            $table->addPrimary($_POST['columnName'.$i]);
        }
            if( isset($_POST['columnCONST'.$i]) && $_POST['columnCONST'.$i] == 'FK'){
                $table->addForeign($_POST['columnName'.$i],$_POST['refTable'.$i],$_POST['refCol'.$i]);
            }

    }

    $table->completeQuery();
    echo $table->getQuery();
    $table->runQuery();
}

if(isset($_POST['tableToFetch'])){
    $columns = [];
    $result = $table->conn->query("SHOW COLUMNS FROM ".$_POST['tableToFetch']);

    if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            $columns[] = $row;
        }
    }
    print_r( json_encode($columns) );
}

