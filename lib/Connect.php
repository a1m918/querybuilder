<?php


class Connect {
    var $conn;
    var $tableName;
    var $addColumn;
    var $addComulmType;

    var $tablesOfDB;

    var $query = '';
    var $CONSTRAINT = ' ';

    function __construct() {
        try {
            $this->conn = mysqli_connect('localhost', 'root', '');
            $this->conn->select_db('healthcare');

            $this->tablesOfDB = $this->conn->query("SHOW TABLES");
            //$tablesOfDB = $tablesOfDB->fetch_row();

        }catch (Exception $e){
            echo $e;
        }

    }


    function getAllDatabases(){
        $results = mysqli_query($this->conn,"SHOW DATABASES");
        return $results;
    }

    function createQuery(){
        $this->query = "CREATE TABLE ".$this->tableName." ( ";
    }

    function addColumn($column,$type,$length=null){
        $this->query .= $column." ".$type.(($length)?"(".$length.")":"").",";
    }

    function addPrimary($column){
        $this->query = rtrim($this->query,",");
        $this->CONSTRAINT .= " ,Primary key (".$column.")";
    }

    function addForeign($column, $refTable, $refColumn){
        $this->CONSTRAINT .= " ,CONSTRAINT Foreign key (".$column.") References ".$refTable."(".$refColumn.")";
    }

    function completeQuery(){
       $this->query = rtrim($this->query,",");
       $this->query.= $this->CONSTRAINT;
       $this->query = rtrim($this->query,",");
       $this->query .= " )";
    }

    function getQuery(){
        return $this->query;
    }

    function addCheck($checkValue){
        $this->query = rtrim($this->query,",");
        $this->query .= " ,CHECK (".$checkValue."),";
    }

    function runQuery(){
        $result = '';
        try {
            $this->conn->query($this->query);
        }catch (Exception $e){
            echo $result;
            echo $e;
        }
    }

}
