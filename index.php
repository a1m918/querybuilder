<?php
    require_once '/lib/create.php';

?>

<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/assets/bootstrap.min.css">

    <script src="/assets/jquery-3.2.1.min.js"></script>
    <script src="/assets/popper.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>

    <script src="/assets/Scripts/main.js"></script>

</head>


<body class="container-fluid">
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Query Builder</a>
        </div>

    </div>
</nav>

<div class="row">
    <div class="col-lg-12">
        <form class="form-horizontal" method="post" action="/lib/create.php">
            <fieldset>


                <legend>Create Table</legend>


                <div class="form-group">
                    <div class="col-md-2">
                        <input id="" name="tableName" type="text" placeholder="Table Name"
                               class="form-control input-md"
                               required="">
                        <span class="help-block">Enter Table Name</span>
                    </div>
                </div>
<div id="columns">

                <div class="form-group">
                    <div class="col-md-2">
                        <input  name="columnName1" type="text" placeholder="Column Name"
                               class="form-control input-md"
                               required="">
                        <span class="help-block">Enter Column Name</span>
                    </div>
                    <div class="col-md-1">
                        <select name="columnType1" class="form-control" required>
                            <option value="int">
                                INT
                            </option>
                            <option value="varchar">
                                VARCHAR
                            </option>
                            <option value="text">
                                TEXT
                            </option>
                        </select>
                        <span class="help-block">Column Type</span>
                    </div>

                    <div class="col-md-1">
                        <input  name="columnLength1" type="number" placeholder="Length"
                               class="form-control input-md"
                               required="">
                        <span class="help-block">Length</span>
                    </div>

                    <div class="col-md-2">
                        Apply Check : <input id="applyChecks1" class="form-inline" type="checkbox" name="applyCheck1">
<!--                        <span>-->
<!--                            <select class="form-control" name="checkType1">-->
<!--                                <option>1</option>-->
<!--                                <option>2</option>-->
<!--                                <option>3</option>-->
<!--                            </select>-->
<!--                        </span>-->
                        <span class="help-block">
                            CHECK(<input type="text" class="form-control" name="checkVal1">)
                                <span class="small">
                                    Enter Value for check
                                </span>
                        </span>
                    </div>

                    <div class="col-md-1">
                        <Select class="form-control form-inline" name="columnCONST1">
                            <option value=""></option>
                            <option value="PK">PRIMARY</option>
                            <option value="FK">FOREIGN</option>
                        </Select>
                        <span class="help-block">Constraint Type</span>
                    </div>

                    <div class="col-md-2">
                        <Select id="refTable1"  class="form-control form-inline" name="refTable1" onchange="callForColumnList(event,this)">
                            <option></option>
                            <?php if($table->tablesOfDB->num_rows > 0)
                                while($row = $table->tablesOfDB->fetch_row()){
                                ?>
                                    <option><?php print_r($row[0]);?></option>

                            <?php }?>
                        </Select>
                        <span class="help-block">Reference Table</span>

                    </div>

                    <div class="col-md-2">
                        <Select class="form-control form-inline" name="refCol1" id="refCol1">

                        </Select>
                        <span class="help-block">Reference Column</span>

                    </div>



                </div>
</div>

                <input type="button" class="btn btn-primary" id="addAnotherColumn" value="Add another Column" >
            </fieldset>

<hr>
        <button id="submit" class="btn btn-primary pull-right" name="create" value="table">Create Table</button>
        </form>
    </div>
</div>
</body>

</html>